" I have a very small neovim configuration file. I have found that vim has the
" tools on its own if you are willing to learn ;p. This is a simple
" replacement for the default hjkl arrow keys (which were chosen for simple
" historical reasons and not any after-the-fact ergonomic justifications) with
" the more natural, i3-approved jkl;.

noremap ; l
noremap l k
noremap k j
noremap j h
